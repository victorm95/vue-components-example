new Vue({
  el: '#app',
  data: {
    showMessage: false,
    options: {
      gender: [
        { name: 'Femenino', value: 'F' },
        { name: 'Masculino', value: 'M' }
      ]
    },
    form: {
      name: null,
      birthday: null,
      gender: null
    }
  }
});
