Vue.component('ui-dropdown', {
  props: {
    options: { type: Array, required: true },
    value: { type: String } // Value para v-model
  },
  template: '<select></select>',
  data: function() {
    return {
      plugin: null,
    };
  },
  watch: {
    value: function(newValue) { // Observar cambios en v-model para modificar el dropdown
      this.plugin.dropdown('set selected', newValue);
    }
  },
  mounted: function() {
    var self = this;

    this.plugin = $(this.$el).dropdown({
      values: this.options,
      onChange: function(value) {
        self.$emit('input', value); // Emitir evento para v-model
      }
    });
  }
});
