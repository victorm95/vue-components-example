Vue.component('date-pickr', {
  props: {
    value: { type: String },
    options: {
      type: Object,
      default: function() {
        return {
          altInput: true,
          locale: 'es',
        };
      }
    }
  },
  template: '<input type="date" v-model="date" />',
  data: function() {
    return {
      pickr: null,
      date: null
    };
  },
  watch: {
    date: function(newValue) {
      this.$emit('input', newValue);
    },
    value: function(newValue) {
      this.pickr.setDate(newValue);
    }
  },
  mounted: function() {
    // Flatpickr no requiere jQuery, pero puede usarse con jQuery. $.flatpickr
    this.pickr = flatpickr(this.$el, this.options);
  },
  beforeDestroy: function() {
    // Algunos plugins tienen la posibilidad de destruirlos
    // Es buena practicar hacerlo antes de destruir el componente
    this.pickr.destroy();
    this.pickr = null;
  }
});
